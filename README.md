> ### Build a simple blog platform, that allow registered users could CRUD their posts and view others. Admin is required to publish users’ posts.

----------

# Getting started

## Server Requirements

You will need to make sure your server meets the following requirements:

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

## Installation

Clone the repository

    git clone https://sstar297@bitbucket.org/sstar297/simple-blog-platform.git

Switch to the repo folder

    cd simple-blog-platform

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Set the database connection in .env before migrating

- DB_HOST (default: 127.0.0.1)
- DB_PORT (default: 3306)
- DB_DATABASE (your database name)
- DB_USERNAME
- DB_PASSWORD
    
Run the database migrations

    php artisan migrate
    
Generate a seeder

    php artisan db:seed

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

## Running the tests

    phpunit
    
## Authors

- Nguyen Hoang Son - [Sstar297](https://bitbucket.org/sstar297)

----------
