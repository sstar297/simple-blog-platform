<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('detail/{id}', 'HomeController@detail')->name('detail');

Route::middleware(['auth'])->prefix('manage/blog')->group(function () {
    Route::get('/', 'BlogController@index')->name('manage.blog.index');
    Route::get('create', 'BlogController@create')->name('manage.blog.create');
    Route::post('store', 'BlogController@store')->name('manage.blog.store');
    Route::get('show/{id}', 'BlogController@show')->name('manage.blog.show');
    Route::get('edit/{id}', 'BlogController@edit')->name('manage.blog.edit')->middleware('author');
    Route::patch('update/{id}', 'BlogController@update')->name('manage.blog.update')->middleware('author');
    Route::delete('destroy/{id}', 'BlogController@destroy')->name('manage.blog.destroy')->middleware('author');
    Route::patch('publish/{id}', 'BlogController@publish')->name('manage.blog.publish')->middleware('admin');
});
