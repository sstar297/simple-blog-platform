<?php
/**
 * Created by PhpStorm.
 * User: sonk5
 * Date: 2019-02-24
 * Time: 11:21 PM
 */

namespace Tests\Unit;


use App\Blog;
use App\User;
use Tests\TestCase;
use Faker\Factory as Faker;

class BlogUnitTest extends TestCase
{
    /** @test */
    function create()
    {
        $faker = Faker::create();
        $data = [
            'user_id' => User::all()->random()->id,
            'title' => $faker->sentence,
            'description' => $faker->text,
            'content' => $faker->paragraph,
        ];

        $post = Blog::create($data);

        $found = Blog::find($post->id);

        $this->assertInstanceOf(Blog::class, $found);
        $this->assertEquals($data['user_id'], $found->user_id);
        $this->assertEquals($data['title'], $found->title);
        $this->assertEquals($data['description'], $found->description);
        $this->assertEquals($data['content'], $found->content);
        $this->assertEquals('unpublish', $found->status);
        $this->assertEquals(null, $found->published_at);
    }

    /** @test */
    function edit()
    {
        $faker = Faker::create();
        $post = factory(Blog::class)->create();

        $data = [
            'title' => $faker->sentence,
            'description' => $faker->text,
            'content' => $faker->paragraph,
        ];

        $post->update($data);

        $this->assertEquals($data['title'], $post->title);
        $this->assertEquals($data['description'], $post->description);
        $this->assertEquals($data['content'], $post->content);
    }

    /** @test */
    function destroy()
    {
        $post = factory(Blog::class)->create();

        $this->assertTrue($post->delete());
    }

    /** @test */
    function publish()
    {
        $faker = Faker::create();
        $data = [
            'user_id' => User::all()->random()->id,
            'title' => $faker->sentence,
            'description' => $faker->text,
            'content' => $faker->paragraph,
        ];

        $post = Blog::create($data);

        $admin = User::where('role', 'admin')->first();

        $publish_timer_at = null;

        $data = [
            'status' => 'published',
            'admin_id' => $admin->id,
            'publish_timer_at' => null,
        ];
        if (!empty($publish_timer_at)) {
            $data['publish_timer_at'] = date('Y-m-d H:i:s', strtotime($publish_timer_at));
            if (strtotime($data['publish_timer_at']) <= time()) {
                $data['published_at'] = $data['publish_timer_at'];
            }
        } else {
            $data['published_at'] = date('Y-m-d H:i:s');
        }

        $post->update($data);

        $this->assertEquals($data['status'], $post->status);
        $this->assertEquals($data['admin_id'], $post->admin_id);
        $this->assertEquals($data['publish_timer_at'], $post->publish_timer_at);
        $this->assertEquals($data['published_at'], $post->published_at);

        $data = [
            'status' => 'unpublish',
            'admin_id' => null,
            'publish_timer_at' => null,
            'published_at' => null,
        ];
        $post->update($data);

        $this->assertEquals($data['status'], $post->status);
        $this->assertEquals($data['admin_id'], $post->admin_id);
        $this->assertEquals($data['publish_timer_at'], $post->publish_timer_at);
        $this->assertEquals($data['published_at'], $post->published_at);
    }
}
