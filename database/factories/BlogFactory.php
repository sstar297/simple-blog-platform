<?php

use Faker\Generator as Faker;

$factory->define(App\Blog::class, function (Faker $faker) {
    $chose_random = rand(0, 1);
    $chose_random_2 = rand(0, 1);
    $status = ['published', 'unpublish'];
    $admin_id = [\App\User::where('role', 'admin')->first()->id, null];
    $publish_timer_at = $chose_random == 0 ? ($chose_random_2 == 0 ? $faker->dateTimeThisYear('+2 month') : null) : null;
    if ($chose_random == 0) {
        if (is_null($publish_timer_at)) {
            $published_at = $faker->dateTimeThisYear();
        } else {
            $published_at = $publish_timer_at->getTimestamp() > time() ? null : $publish_timer_at;
        }
    } else {
        $published_at = null;
    }
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'title' => $faker->sentence,
        'description' => $faker->text,
        'content' => $faker->paragraph,
        'status' => $status[$chose_random],
        'admin_id' => $admin_id[$chose_random],
        'publish_timer_at' => $publish_timer_at,
        'published_at' => $published_at,
    ];
});
