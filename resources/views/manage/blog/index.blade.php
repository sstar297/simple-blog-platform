@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <form method="get" action="{{ route('manage.blog.index') }}">
                    <div class="card">
                        <div class="card-header">Filter</div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="all" {{ request('status')=='all'?'selected':'' }}>All Post</option>
                                    <option value="published" {{ request('status')=='published'?'selected':'' }}>Published Post</option>
                                    <option value="unpublish" {{ request('status')=='unpublish'?'selected':'' }}>Unpublished Post</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Created Date From</label>
                                <input type="date" class="form-control" name="from" value="{{ request('from') }}">
                            </div>
                            <div class="form-group">
                                <label>Created Date To</label>
                                <input type="date" class="form-control" name="to" value="{{ request('to') }}">
                            </div>

                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">Order</div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Order By</label>
                                <select class="form-control" name="orderby">
                                    <option value="created_at" {{ request('orderby')=='created_at'?'selected':'' }}>Created date</option>
                                    <option value="published_at" {{ request('orderby')=='published_at'?'selected':'' }}>Published date</option>
                                    <option value="status" {{ request('orderby')=='status'?'selected':'' }}>Status</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Order Type</label>
                                <select class="form-control" name="ordertype">
                                    <option value="asc" {{ request('ordertype')=='asc'?'selected':'' }}>Ascending Order</option>
                                    <option value="desc" {{ request('ordertype')=='desc'?'selected':'' }}>Descending Order</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">List of blog</div>

                    <div class="card-body table-responsive">
                        <a href="{{ route('manage.blog.create') }}" class="btn btn-primary">Create new post</a>
                        {{ $posts->appends(request()->except('page'))->links() }}
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                @if(Auth::user()->role=='admin')
                                    <th scope="col">Admin Action</th>
                                @endif
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Status</th>
                                <th scope="col">Author</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    @php($first=!empty(request('page'))?(request('page')-1)*config('blog.per_page'):0)
                                    <th scope="row">{{ $loop->iteration+$first }}</th>
                                    @if(Auth::user()->role=='admin')
                                        <td class="{{ $post->status=='unpublish'?'unpublish':'' }}">
                                            <a href="javascript:void(0)" onclick="showForm('publish-post-{{ $post->id }}')">{{ $post->status=='unpublish'?'Publish':'Unpublish' }}</a>

                                            <form id="publish-post-{{ $post->id }}" action="{{ route('manage.blog.publish',['id'=>$post->id]) }}" method="POST" class="d-none">
                                                {{ method_field('patch') }}
                                                {{ csrf_field() }}
                                                @if($post->status=='unpublish')
                                                    <input type="datetime-local" name="publish_timer_at">
                                                @endif
                                                <button type="submit">Submit</button>
                                            </form>
                                        </td>
                                    @endif
                                    <td>
                                        @if($post->user_id == Auth::id())
                                            <a href="{{ route('manage.blog.edit',['id'=>$post->id]) }}">{{ $post->title }}</a>
                                        @else
                                            {{ $post->title }}
                                        @endif
                                    </td>
                                    <td>{{ $post->description }}</td>
                                    <td>{{ ucfirst($post->status) }} </td>
                                    <td>{{ $post->getUser->name }}</td>
                                    <td>
                                        <a href="{{ route('manage.blog.show',['id'=>$post->id]) }}" target="_blank">View</a>
                                        @if($post->user_id == Auth::id())
                                            <a href="{{ route('manage.blog.edit',['id'=>$post->id]) }}">Edit</a>
                                            <br>
                                            <a href="{{ route('manage.blog.destroy',['id'=>$post->id]) }}"
                                               onclick="event.preventDefault(); document.getElementById('destroy-post-{{ $post->id }}').submit();">
                                                Delete
                                            </a>

                                            <form id="destroy-post-{{ $post->id }}" action="{{ route('manage.blog.destroy',['id'=>$post->id]) }}" method="POST" style="display: none;">
                                                {{ method_field('delete') }}
                                                {{ csrf_field() }}
                                            </form>
                                        @endif


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $posts->appends(request()->except('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function showForm(id) {
            var element = document.getElementById(id);
            element.classList.remove("d-none");
        }
    </script>
@endsection
