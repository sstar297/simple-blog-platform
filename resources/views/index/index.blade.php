@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                {{ $posts->links() }}
                @foreach($posts as $post)
                    <div class="card">
                        <div class="card-header"><a href="{{ route('detail',['id'=>$post->id]) }}">{{ $post->title }}</a></div>

                        <div class="card-body">
                            <p>Author: {{ $post->getUser->name }} ({{ $post->published_at }})</p>
                            <p class="card-title">{{ $post->description }}</p>
                        </div>
                    </div>
                @endforeach
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection
