<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $posts = new Blog();
        if (Auth::user()->role == 'user') {
            $posts = $posts->where('user_id', Auth::id());
        }
        if (!empty($request->status) && $request->status != 'all') {
            $posts = $posts->where('status', $request->status);
        }
        if (!empty($request->from)) {
            $posts = $posts->whereDate('created_at', '>=', $request->from);
        }
        if (!empty($request->to)) {
            $posts = $posts->whereDate('created_at', '<=', $request->to);
        }
        $orderby = empty($request->orderby) || Schema::hasColumn(app(Blog::class)->getTable(), $request->orderby) != true ? 'created_at' : $request->orderby;
        $ordertype = empty($request->ordertype) || !in_array($request->ordertype, ['desc', 'asc']) ? 'asc' : $request->ordertype;

        $posts = $posts->orderBy($orderby, $ordertype)->paginate(config('blog.per_page'));
        return view('manage.blog.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('manage.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);
        $data = [
            'user_id' => Auth::id(),
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
        ];
        Blog::create($data);
        return redirect(route('manage.blog.index'))->with('success', 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     */
    public function show($id)
    {
        $post = Blog::findOrFail($id);
        if ($post->user_id != Auth::id() && Auth::user()->role != 'admin') {
            abort(404);
        }
        return view('index.detail', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     */
    public function edit($id)
    {
        $post = Blog::findOrFail($id);
        return view('manage.blog.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);
        $post = Blog::findOrFail($id);
        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
        ];
        $post->update($data);
        return redirect(route('manage.blog.index'))->with('success', 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     */
    public function destroy($id)
    {
        $post = Blog::findOrFail($id);
        $post->delete();
        return redirect(route('manage.blog.index'))->with('success', 'Success');
    }

    public function publish(Request $request, $id)
    {
        $post = Blog::findOrFail($id);
        if ($post->status == 'unpublish') {
            $data = [
                'status' => 'published',
                'admin_id' => Auth::id(),
                'publish_timer_at' => null,
            ];
            if (!empty($request->publish_timer_at)) {
                $data['publish_timer_at'] = date('Y-m-d H:i:s', strtotime($request->publish_timer_at));
                if (strtotime($data['publish_timer_at']) <= time()) {
                    $data['published_at'] = $data['publish_timer_at'];
                }
            } else {
                $data['published_at'] = date('Y-m-d H:i:s');
            }

        } else {
            $data = [
                'status' => 'unpublish',
                'admin_id' => null,
                'publish_timer_at' => null,
                'published_at' => null,
            ];
        }
        $post->update($data);
        return redirect()->back()->with('success', 'Success');
    }
}
