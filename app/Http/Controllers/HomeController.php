<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the list of posts.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Blog::whereNotNull('published_at')->orderBy('published_at', 'desc')->paginate(config('blog.per_page'));
        return view('index.index', compact('posts'));
    }

    /**
     * Show the detail of post.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $post = Blog::findOrFail($id);
        return view('index.detail', compact('post'));
    }
}
