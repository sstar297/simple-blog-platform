<?php

namespace App\Http\Middleware;

use App\Blog;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $post = Blog::findOrFail($request->route('id'));
        if ($post->user_id != Auth::id()) {
            abort(404);
        }
        return $next($request);
    }
}
