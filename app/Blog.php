<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'content',
        'status',
        'admin_id',
        'publish_timer_at',
        'published_at',
    ];

    function getUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    function getAdmin()
    {
        return $this->belongsTo(User::class, 'published_user_id');
    }
}
